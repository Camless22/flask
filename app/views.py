from flask import render_template
from . import main


@main.route('/')
def splash():
    return render_template('splash.html')


@main.route('/models')
def models():
    return render_template('models.html')


@main.route('/state/<int:id>')
def state():
    # state = State.query.get_or_404(id)  # Fetch the particular state/nation/center we want.
    # return render_template('states.html', state=state) Uncomment when we have some samples to play with.
    return render_template('states.html')


@main.route('/center/<int:id>')
def centers():
    # center = DetentionCenter.query.get_or_404(id)
    return render_template('centers.html')


@main.route('/nation/<int:id>')
def nations():
    # nation = Nation.query.get_or_404(id)
    # return render_template('nations.html', nation=nation)
    return render_template('nations.html')

